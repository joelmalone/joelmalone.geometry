﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Geometry
{

    public struct Edge
    {

        public Vector3 V1 { get; set; }
        public Vector3 V2 { get; set; }

    }

    public static class EdgeExtensions
    {

        public static IEnumerable<Vector3> EnumerateVertices(this Edge me)
        {
            yield return me.V1;
            yield return me.V2;
        }

        public static IEnumerable<Edge> EnumerateAsEdges(this Triangle me)
        {
            yield return new Edge { V1 = me.V1, V2 = me.V2 };
            yield return new Edge { V1 = me.V2, V2 = me.V3 };
            yield return new Edge { V1 = me.V3, V2 = me.V1 };
        }


    }

}
