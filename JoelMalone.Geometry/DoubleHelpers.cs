﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoelMalone.Geometry
{
    
    public static class DoubleHelpers
    {

        public static double Clamp01(this double value)
        {
            return Clamp(value, 0, 1);
        }

        public static double Clamp(this double value, int min, int max)
        {
            if (value < min)
                return min;
            else if (value > max)
                return max;
            else
                return value;
        }

    }

}
