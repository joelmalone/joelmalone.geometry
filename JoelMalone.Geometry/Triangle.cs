﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Geometry
{
    
    public struct Triangle
    {
        public Vector3 V1 { get; set; }
        public Vector3 V2 { get; set; }
        public Vector3 V3 { get; set; }
    }

    public static class TriangleExtensions
    {

        public static IEnumerable<Vector3> EnumerateVertices(this Triangle me)
        {
            yield return me.V1;
            yield return me.V2;
            yield return me.V3;
        }

        public static IEnumerable<Vector3> EnumerateVertices(this IEnumerable<Triangle> me)
        {
            return me.SelectMany(t => t.EnumerateVertices());
        }

    }

}
