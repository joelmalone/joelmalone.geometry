﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Geometry
{
    
    public struct Extents
    {
        public Vector3 Min { get; set; }
        public Vector3 Max { get; set; }

        public override string ToString()
        {
            return string.Format("Min: {0} Max: {1}", Min, Max);
        }
    }

    public static class ExtentsExtensions
    {

        public static Extents Include(this Extents extents, Vector3 point)
        {
            extents.Min = new Vector3(
                Math.Min(extents.Min.X, point.X),
                Math.Min(extents.Min.Y, point.Y),
                Math.Min(extents.Min.Z, point.Z)
                );
            extents.Max = new Vector3(
                Math.Max(extents.Min.X, point.X),
                Math.Max(extents.Min.Y, point.Y),
                Math.Max(extents.Min.Z, point.Z)
                );
            return extents;
        }

        public static Vector3 GetMid(this Extents extents)
        {
            return (extents.Min + extents.Max) * .5;
        }

    }

}
