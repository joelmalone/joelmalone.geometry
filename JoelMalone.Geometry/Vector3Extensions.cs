﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Geometry
{
    
    public static class Vector3Extensions
    {

        public static Extents ToExtents(this IEnumerable<Vector3> points)
        {
            var min = points.First();
            var max = min;
            foreach (var p in points)
            {
                if (p.X < min.X)
                    min.X = p.X;
                else if (p.X > max.X)
                    max.X = p.X;
                
                if (p.Y < min.Y)
                    min.Y = p.Y;
                else if (p.Y > max.Y)
                    max.Y = p.Y;
                
                if (p.Z < min.Z)
                    min.Z = p.Z;
                else if (p.Z > max.Z)
                    max.Z = p.Z;
            }
            return new Extents
            {
                Min = min,
                Max = max,
            };
        }

    }

}
